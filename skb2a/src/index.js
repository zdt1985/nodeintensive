const express = require('express');
const app = express();

//отключаем Cors, иначе проверка не проходит
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//проверяем что передано число(функция с https://learn.javascript.ru/number)
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

app.get('/', function (req, res) {
  //присваеваем значения a и b
  if (isNumeric(+req.query.a)) {
    var a = +req.query.a;
  } else {
    var a = 0;
  }
  if (isNumeric(+req.query.b)) {
    var b = +req.query.b;
  } else {
    var b = 0;
  }
  //выводим сумму строкой
  res.send((a + b).toString());
});

app.listen(3000);
