import express from 'express';
import cors from 'cors';

const pattern = /[\s,]+/;
const pattern1 = /[\S]/;
const pattern2 = /[\d]/;
const pattern3 = /[_]/;
const pattern4 = /[/]/;

const app = express();
app.use(cors());
app.get('/task2B', (req, res) => {
  const fullName = (req.query.fullname).trim();
  if (fullName.length === 0 || fullName.match(pattern2) !== null
  || fullName.split(pattern).length >= 4 || fullName.match(pattern3) !== null
  || fullName.match(pattern4) !== null) {
    res.send('Invalid fullname');
  } else if (fullName.split(pattern).length === 3) {
    const surname = fullName.split(pattern)[2] + ' ';
    const name = fullName.split(pattern)[1];
    const n = name.match(pattern1) + '.';
    const patronymic = fullName.split(pattern)[0];
    const p = patronymic.match(pattern1) + '. ';
    res.send(capitaliseFirstLetter((surname.toLowerCase())) + p.toUpperCase() + n.toUpperCase());
  } else if (fullName.split(pattern).length === 2) {
    const surname = fullName.split(pattern)[1] + ' ';
    const name = fullName.split(pattern)[0];
    const n = name.match(pattern1) + '.';
    res.send(capitaliseFirstLetter((surname.toLowerCase())) + n.toUpperCase());
  } else if (fullName.split(pattern).length === 1) {
    const surname = fullName.split(pattern)[0];
    res.send(capitaliseFirstLetter((surname.toLowerCase())));
  }
});

function capitaliseFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
