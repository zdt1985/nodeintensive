import express from 'express';
import cors from 'cors';
import fetch from 'isomorphic-fetch'
import mongoose from 'mongoose';
import Promise from 'bluebird';
import bodyParser from 'body-parser';
mongoose.Promise = Promise;
mongoose.connect('mongodb://publicdb.mgbeta.ru/zdt1985_skb3');

const app = express();

const pcpunkPetspHair = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

let punkPetspHair = {};
fetch(pcpunkPetspHair)
  .then(async (res) => {
    punkPetspHair = await res.json();
  })
  .catch(err => {
    console.log('Чтото пошло не так:', err);
  });

app.use(bodyParser.json());
app.use(cors());

const pattern = /[\S]+/ig;

app.get('/*', async (req, res) => {
  if ((req.url).search(/\?/) !== -1) {
    if ((req.url).search(/pets/) !== -1) {
      const type = req.query.type;
      let response = {};
      for (let i = 0; i < 3; i++) {
        if (punkPetspHair.pets[i].type === 'cat') {
          response.push(punkPetspHair.pets[i]);
        }
      }
      res.send(response);
    }
  } else if (req.url === '/') {
    res.json(punkPetspHair);
  } else if (req.url === '/users') {
    res.json(punkPetspHair.users);
  } else if (req.url === '/pets') {
    res.json(punkPetspHair.pets);
  } else {
      const request = req.url.replace(/\//gi, ' ').match(pattern);
      const requestLength = request.length;
      if (requestLength === 2 && (request[1] - 1) > -1
      && (request[1] - 1) < punkPetspHair[request[0]].length ) {
        const response = punkPetspHair[request[0]][request[1] - 1];
        res.json(response);
      } else {
      res.status(404).send('Not Found');
      }
    }
});

// app.get('/users', async (req, res) => {
//   const users = await User.find();
//   return res.json(users);
// });

// app.get('/pets', async (req, res) => {
//   const pets = await Pet.find().populate('owner');
//   return res.json(pets);
// });
//
// app.post('/data', async (req, res) => {
//   const data = req.body;
//   // console.log(data);
//   // return res.json({
//   //   data
//   // });
//   return res.json(await saveDataInDb(data));
// });

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
