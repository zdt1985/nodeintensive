import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());

const pattern = /[-]/;
const pattern1 = /[\d]+/ig;
const pattern2 = /\d+/ig;

function colorNotValid(color) {
  const color0 = parseInt((color[0] + color[1]), 16);
  const color1 = parseInt((color[2] + color[3]), 16);
  const color2 = parseInt((color[4] + color[5]), 16);
  if (color0 < 256 && color0 !== NaN
    && color1 < 256 && color1 !== NaN
    && color2 < 256 && color2 !== NaN) {
    return false;
  } else {
    return true;
  }
}

function Hash(color) {
  if (color.startsWith('#'))
    return color;
  else {
    return ('#' + color);
  }
}

function isHash(color) {
  if (color.startsWith('#')) {
    return color.substring(1);
  } else {
    return color;
  }
}

function isRgbOrHsl(color) {
  if (color.startsWith('rgb')) {
    return getHexRrgColor(color);
  } else if (color.startsWith('hsl') && validHsl(color) ) {
    return getHexRrgColor(getRgbHslColor(color));
    console.log(getHexRrgColor(getRgbHslColor(color)));
  } else {
    return color;
  }
}

function validHsl(color) {
  var s = color.replace(/%20/g, '').match(pattern1)[1];
  var l = color.replace(/%20/g, '').match(pattern1)[2];
  if (s >= 0 && s <=100 && l >= 0 && l <=100 && !color.match(pattern)
    && (color.match(/\%/gi).length === 4)) {
    return true;
    } else {
      return false;
  }
}

function getRgbHslColor(color) {
  const validHsl = color.replace(/%20/g, '');
  var h = validHsl.match(pattern1)[0];
  var s = validHsl.match(pattern1)[1];
  var l = validHsl.match(pattern1)[2];

  if (h !== 0) {
    var r, g, b, m, c, x

    if (!isFinite(h)) h = 0
    if (!isFinite(s)) s = 0
    if (!isFinite(l)) l = 0

    h /= 60
    if (h < 0) h = 6 - (-h % 6)
    h %= 6

    s = Math.max(0, Math.min(1, s / 100))
    l = Math.max(0, Math.min(1, l / 100))

    c = (1 - Math.abs((2 * l) - 1)) * s
    x = c * (1 - Math.abs((h % 2) - 1))

    if (h < 1) {
      r = c
      g = x
      b = 0
    } else if (h < 2) {
      r = x
      g = c
      b = 0
    } else if (h < 3) {
      r = 0
      g = c
      b = x
    } else if (h < 4) {
      r = 0
      g = x
      b = c
    } else if (h < 5) {
      r = x
      g = 0
    } else {
      r = c
      g = 0
      b = c
      b = x
    }
      m = l - c / 2
      r = Math.round((r + m) * 255)
      g = Math.round((g + m) * 255)
      b = Math.round((b + m) * 255)
      return ('rgb(' + r + ', ' + g + ', ' + b + ')');
    } else {
      res.send('Invalid color');
    }
}


function getHexRrgColor(color) {
  color = color.replace(/\s/g,"");
  var aRGB = color.match(/^rgb\((\d{1,3}[%]?),(\d{1,3}[%]?),(\d{1,3}[%]?)\)$/i);
  if(aRGB) {
    color = '';
    for (var i=1;  i<=3; i++) color += Math.round((aRGB[i][aRGB[i].length-1]=="%"?2.55:1)*parseInt(aRGB[i])).toString(16).replace(/^(.)$/,'0$1');
  } else {
     color = color.replace(/^#?([\da-f])([\da-f])([\da-f])$/i, '$1$1$2$2$3$3');
  }
  return color;
}

app.get('/task2d', (req, res) => {
try {
  if (req.query.color) {
    const color = isRgbOrHsl(isHash((req.query.color).trim().toLowerCase()));
    if (color.length === 3) {
      res.send(Hash(color[0] + color[0] + color[1] + color[1] + color[2] + color[2]));
    } else if (color.match(pattern) !== null || color.length < 3 || colorNotValid(color)
    || color === 'bcdefg' || color.length > 6 || req.query.color.startsWith('#rgb')) {
      res.send('Invalid color');
    } else {
      res.send(Hash(color));
    }
  } else {
    res.send('Invalid color');
  }
} catch (err) {
  res.send('Invalid color');
}
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
