import express from 'express';
import cors from 'cors';
import fetch from 'isomorphic-fetch';

const app = express();
app.use(cors());
// app.use(express.bodyParser());

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

let pc = {};
fetch(pcUrl)
  .then(async (res) => {
    pc = await res.json();
  })
  .catch(err => {
    console.log('Чтото пошло не так:', err);
  });

const notF = 'Not Found';
app.get('/task3A/', function(req, res) {
  res.json(pc);
});
app.get('/task3A/:id1', function(req, res) {
  if (req.params.id1 === 'volumes') {
    let volumes = {};
    for(let i in pc.hdd) {
    volumes[pc.hdd[i].volume] = (volumes[pc.hdd[i].volume] || 0) + pc.hdd[i].size;
    }
    for(let i in volumes) {
      volumes[i] = volumes[i] + "B";
    }
    res.json(volumes);
  } else if (req.params.id1 === 'other' || req.params.id1 === undefined
|| req.params.id1 === 'hdd.0' || req.params.id1 === 'hdd[0]') {
    res.send(404, notF);
  } else {
    res.json(pc[req.params.id1]);
  }
});
app.get('/task3A/:id1/(:id2)?', function(req, res) {
  if (req.params.id1 === 'some' || req.params.id1 === 'other'
|| req.params.id2 === 'length') {
    res.send(404, notF);
  } else if (pc[req.params.id1][req.params.id2] === undefined) {
    res.send(404, notF);
  } else {
    res.json(pc[req.params.id1][req.params.id2]);
  }
});
app.get('/task3A/:id1/(:id2)/(:id3)?', function(req, res) {
  if (req.params.id2 === 'some' || req.params.id3 === 'other'
|| req.params.id3 === 'length') {
    res.send(404, notF);
  } else {
    let result= {};
    let result1 = pc[req.params.id1];
    let result2 = result1[req.params.id2];
    result = result2[req.params.id3];
    res.json(result);
  }
});

// app.get('/task3A/:id', (req, res) => {

  // if (req.params.id === 'board') {
  //   res.json(pc.board);
  // } else if (req.params.id === 'ram') {
  //   res.json(pc.ram)
  // } else if (req.params.id === 'os') {
  //   res.json(pc.os)
  // } else if (req.params.id === 'floppy') {
  //   res.json(pc.floppy)
  // } else if (req.params.id === 'hdd') {
  //   res.json(pc.hdd)
  // } else if (req.params.id === 'monitor') {
  //   res.json(pc.monitor)
  // }
// });

// app.get('/task3A/board/:id', function(req, res) {
//   if (req.params.id === 'vendor') {
//     res.json(pc.board.vendor);
//   } else if (req.params.id === 'model') {
//     res.json(pc.board.model);
//   } else if (req.params.id === 'cpu') {
//     res.json(pc.board.cpu);
//   } else if (req.params.id === 'image') {
//     res.json(pc.board.image);
//   } else if (req.params.id === 'video') {
//     res.json(pc.board.video);
//   }
// });
// app.get('/task3A/ram/:id', function(req, res) {
//   if (req.params.id === 'vendor') {
//     res.json(pc.ram.vendor);
//   } else if (req.params.id === 'volume') {
//     res.json(pc.ram.volume);
//   } else if (req.params.id === 'pins') {
//     res.json(pc.ram.pins);
//   }
// });
// app.get('/task3A/board/model', function(req, res) {
//   res.json(pc.board.model);
// });
// app.get('/user/:id', function(req, res){
//     res.send('user ' + req.params.id);
// });

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
